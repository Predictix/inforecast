import json

from lbconfig.api import *
#from lbconfig import modeler

import modeler as modeler

import os

#-----------------------------------------------------------------------------#
# Project Configuration and Dependencies
#-----------------------------------------------------------------------------#
basedir=os.path.dirname(os.path.realpath(__file__))

# Configuration
lbconfig_package(
  name="inforecast",
  version = "1.0",
  default_prefix = "out",
  default_targets = ["lb-libraries", "archive-ws-ui", "archive-ws-data"]
)

# Dependencies
depends_on(
  logicblox_dep,
  lb_web_dep,
  measure_service={
     'default_path': '$(LB_MEASURE_SERVICE_HOME)',
     'help': 'Location of bloxweb measure service used by this build'
  },
  networkManager = {
    'default_path' : '$(NETWORK_MANAGER_HOME)',
    'help' : 'Network Manager.'
  },
  modelerjs='$(LOGICBLOX_HOME)',
  lb_workflow='$(LB_WORKFLOW_HOME)',
  lb_web_workbooks='$(LB_WORKBOOK_SERVICE_HOME)'
)

external_lib_deps = {
  "lb_web" : "$(lb_web)",
  "lb_measure_service" : "$(measure_service)"
}

variable('modelerjs_py','$(LOGICBLOX_HOME)/python')

#-----------------------------------------------------------------------------#
# Helper functions
#-----------------------------------------------------------------------------#
def load_metamodel(module):
    return [  "sed 's|cleancsvs|cleancsvs/%s|g' scripts/batch/config.batch >/tmp/%s.batch" % (module, module),
              "lb web-client batch -i /tmp/%s.batch" % module ]

def populate_metamodel(ws='data'):
    commands=["lb web-server load-services -w %s" % ws]
    
    for module in modeler.modeler_projects:
        commands.extend(load_metamodel(module))
    
    commands.extend([ "lb execblock %s modeler_config:metamodel:translate" % ws,
                      "lb execblock %s modeler_config:services:workbook" % ws,
                      "lb web-server load-services -w %s" % ws])

    return commands
def load_modeler_ui(prefix='ip-workspace'):
    return [ "python $(modelerjs_py)/install_ui.py %s" % prefix ]

# modeler in modules

modeler.modeler_library(name='data_model',
                        config_dir='src/config/model/data_model')

modeler.configure('ip-workspace',
                  libraries=['data_model'],
                  app_base_dir=basedir,
                  js_scripts_dest_dir='%s/scripts/js' % basedir,
                  js_lib_dest_dir='%s/node_modules' % basedir)


modeler_libraries=[
  'ip_config'
]

for lib_name in modeler_libraries:
    lb_library( name=lib_name,
                srcdir='src/modeler',
                deps =external_lib_deps )

lb_library( name='ip_workspace', 
            srcdir='src/logiql',
            generated = False,
            deps = {'lb_web': '$(lb_web)',
                    'lb_workflow': '$(lb_workflow)',
                    'lb_web_workbooks': '$(lb_web_workbooks)',
                    'lb_measure_service': '$(lb_measure_service)',
                    'pivot': '$(modelerjs)',
                    'modeler_config': '$(modelerjs)'} )

# Libraries
libraries = [
  { "name": "model", "path": "src/common" },
  { "name": "meta", "path": "src/common" },
  { "name": "common_tdx", "path": "src/common" },
  { "name": "common_tdx_helpers", "path": "src/common/common_tdx" },
  { "name": "measure", "path": "src/logiql/services/protobuf" },
  { "name": "global_measure", "path": "src/logiql/services/protobuf" },
  { "name": "proxy", "path": "src/logiql/services/protobuf" },
  { "name": "protobuf", "path": "src/logiql/services" },
  { "name": "services", "path": "src/logiql" },
  { "name": "inforecast_netmap", "path": "src/logiql/services/tdx" },
  { "name": "tdx", "path": "src/logiql/services" },
  { "name": "data_workspace", "path": "src/logiql" },
  { "name": "ui_workspace", "path": "src/logiql" }
]

for lib in libraries:
  lb_library(
    name = lib["name"],
    srcdir = lib["path"],
    deps = external_lib_deps
  )

# Install
ws_archive(name='data', libraries=['ip_workspace'], compress=False)
ws_archive(name='ui', libraries=[ 'ui_workspace' ], compress=False)

#-----------------------------------------------------------------------------#
# populate metamodel
#-----------------------------------------------------------------------------#
rule( output='$(build)/populate_metamodel.success',
      input='$(build)/check_workspace_ws-data.success',
      commands=(populate_metamodel() + ['touch $(build)/populate_metamodel.success']) )
#-----------------------------------------------------------------------------#
# load authorization config
# -----------------------------------------------------------------------------#
rule( output='$(build)/load_authorization_config.success',
      input='$(build)/populate_metamodel.success',
      commands=['lb web-client batch -i scripts/batch/authorization_config.batch',
                'touch $(build)/load_authorization_config.success' ] )
#-----------------------------------------------------------------------------#
# load modeler ui (sheets, canvases, navigation)
#-----------------------------------------------------------------------------#
rule( output='$(build)/load_modeler_ui.success',
      input='$(build)/load_authorization_config.success',
      commands=(load_modeler_ui() + ['touch $(build)/load_modeler_ui.success']) )
#-----------------------------------------------------------------------------#
# generate modeler warmup queries
#-----------------------------------------------------------------------------#
rule( output='$(build)/generate_modeler_warmup_queries.success',
      input=['$(build)/load_modeler_ui.success'],
      commands=['make view-queries',
                'make rule-queries',
                'touch $(build)/generate_modeler_warmup_queries.success'] )
# #-----------------------------------------------------------------------------#
# # add compiled library data_workspace to the data workspace and load services
# #-----------------------------------------------------------------------------#
rule( output='$(build)/add_data_workspace_lib.success',
      input=[ '$(build)/generate_modeler_warmup_queries.success',
              '$(build)/sepcomp/data_workspace' ],
      commands=['lb addproject data $(build)/sepcomp/data_workspace --libpath $(lb_web)/share:$(build)/sepcomp:$(measure_service)/share',
                'lb web-server load-services -w data',
                'touch $(build)/add_data_workspace_lib.success'] )
# #-----------------------------------------------------------------------------#
# # call metamodel_extension service
# #-----------------------------------------------------------------------------#
rule( output='$(build)/call_metamodel_extension.success',
      input='$(build)/add_data_workspace_lib.success',
      commands=['echo "{}" | lb web-client call "http://localhost:8080/metamodel_extension"',
                'touch $(build)/call_metamodel_extension.success'] )
# #-----------------------------------------------------------------------------#
# # branching for workbooks
# #-----------------------------------------------------------------------------#
rule( output='$(build)/branch_for_workbooks.success',
      input=[ '$(build)/call_metamodel_extension.success' ],
      commands=['bash %s/scripts/branch-for-workbooks.sh %s %s %s' % (basedir, 'data', '__workbook_base_branch', 'IP'),
                'touch $(build)/branch_for_workbooks.success'] )

rule( output='$(build)/code_generation.success',
      input=[ '$(build)/branch_for_workbooks.success' ],
      commands=['mkdir -p $(build)/code_generation',
                'bash scripts/code_generation/find_EDB.sh data __workbook_base_branch_IP',
                'touch $(build)/code_generation.success'] )

rule('IP-workbook',
 input=['$(build)/IP-workbook.success'],
     commands=[])

rule(output = '$(build)/IP-workbook.success',
     input=[],
     commands=[
         'bash scripts/branch-for-workbooks-after-data-load.sh /20160803/partition __workbook_base_branch IP',
         'make create-templates',
         'make build-all-workbooks',
         'touch $(build)/IP-workbook.success'])

rule('create-templates',
     input=[],
     commands=[
         'lb workflow run -q --file $(prefix)/workflow/templates.wf --main workbooks.create_templates --param app_prefix="20160803/partition" --param src_root=%s --max-retries 2' % ( basedir ),
         'lb branches /20160803/partition | grep tpl | while read line; do lb exec /20160803/partition@$$line \'^meta:workspaces:me[] ="/20160803/partition".\'; done'],
     phony=True)

rule(output = 'build-all-workbooks',
     input=[],
     commands=[
         'lb workflow run -q --file $(prefix)/workflow/workbooks.wf --main workbooks.build_all_workbooks --max-retries 2',
         'lb workbook list -w /20160803/partition --csv id | xargs -P 1 -n 1 lb workbook add-users -w /20160803/partition -u user1 --id'],
     phony=True)

rule('start-nginx', [], ['sudo nginx -g "user %s staff;" -p $(PWD)/ -c conf/nginx.conf | grep -v "could not open error log file"' % os.getenv("USER")])

# #-----------------------------------------------------------------------------#
# # do not tar-up until branch_for_workbooks.success is emitted
# #-----------------------------------------------------------------------------#
rule( output='$(build)/workspaces/data',
      input=[ '$(build)/code_generation.success' ] )

rule(
    output = 'install',
    input = [
      '$(build)/workspaces/data',
      '$(build)/workspaces/ui'
    ],
    commands = [
      'mkdir -p $(prefix)/workspaces',
      'cp -r $(build)/workspaces/* $(prefix)/workspaces/'
    ]
)
# Folders
install_dir('src/frontend', 'static')
install_dir('src/ui/', 'ui')
install_dir('src/ui/measure_views/', 'ui/measure_views')
install_dir('config', 'config')
install_dir('tests', 'tests')
install_dir('$(networkManager)', 'network-manager')
install_dir('src/workflow', 'workflow')


# Files
install_file('$(measure_service)/lib/java/handlers/lb-measure-service.jar', '')
install_file('install.sh', '')
install_file('config/lb-web-server.config', '')
install_file('scripts/wf_wrapper.py', '')


# Deploy
rule(
  output='deploy',
  input=[],
  commands=[
    'make run-workflow-bootstrap',
    'make run-workflow-dev-deploy'
  ]
)

# Clean
rule(
  output = "clean",
  input = [],
  commands = ['$(Q)rm -rf dist build Makefile *.pyc'],
  phony = True
)

# Test
rule(
  output = "check",
  input = [],
  commands = ['lamias tests/tests.js'],
  phony = True
)

# update Test
rule(
  output = "update-tests",
  input = [],
  commands = ['lamias -m update tests/tests.js'],
  phony = True
)

#-----------------------------------------------------------------------------#
# Deploy using network-manager
#-----------------------------------------------------------------------------#
lb_workflow_run(
  workflow_file='$(prefix)/workflow/main/bootstrap_network_manager.wf',
  name='bootstrap',
  extensions=["$(prefix)/network-manager"],
  bindings=[
    'target_dir=$(prefix)',
    'network_path=$(prefix)/network-manager',
    'workspaces=$(prefix)/workspaces',
    'config_dir=$(prefix)/config',
    'data_dir=data',
    'gen_zero=20160803',
    'extra_config_dir=$(prefix)/config'])

lb_workflow_run(
  workflow_file='$(prefix)/workflow/dev_deploy.wf',
  name='dev-deploy',
  extensions=["$(prefix)/network-manager"],
  bindings=[
    'target_dir=$(prefix)',
    'network_path=$(prefix)/network-manager',
    'workspaces=$(prefix)/workspaces',
    'config_dir=$(prefix)/config',
    'data_dir=data',
    'gen_zero=20160803',
    'stagenode=localhost',
    'extra_config_dir=$(prefix)/config'])