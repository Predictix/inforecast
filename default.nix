{ nixpkgs ? <nixpkgs>
, builder_config ? import <config> { inherit nixpkgs; }
, platform_release ? "4.4.5.latest"
, lamias_src ? ../lamias-servicetester
, measure_query_gen_src ? ../measure-query-gen
, src_inforecast ? ./.
, pdxops ? null
, network_manager_src ? ../network_manager
}:

let
  pkgs = builder_config.pkgs;

  logicblox = builder_config.getLB platform_release;

  dev_data = builder_config.fetchs3 {
    url = "s3://logicblox-private/platform.predictix.com/test_data/inforecast_dev_data8.tgz";
    sha256 = "2714963198a3f4c6b100375aa19da293a09786434914a44179717502c9a84515";
  };

  measure_query_gen = import measure_query_gen_src {};

  lamias = import lamias_src { inherit pkgs; };

  network_manager = 
    import network_manager_src { inherit platform_release; };
in
  builder_config.genericAppJobset {

    inherit logicblox;
    
    build = builder_config.buildLBConfigNodeApp rec {
      nodePackages = ./node-packages-generated.nix;
      name = "inforecast";
      src = if pkgs.lib.inNixShell then null else src_inforecast;
      buildInputs = [ logicblox pkgs.python27Packages.pystache];
      propagatedBuildInputs = [ pkgs.nodejs pkgs.curl pkgs.zip];
      configureFlags = [];

      preConfigure = ''
        export MEASURE_QUERY_GEN_HOME=${measure_query_gen.package}/lib/node_modules/measure-query-gen
        export PATH=${lamias.build}/lib/node_modules/lamias/bin:$PATH
        export MEASURE_QUERY_GEN_VIEWS=$out/ui/measure_views/gridsConfig.js
        export NETWORK_MANAGER_HOME=${network_manager.build}
        export PATH=${network_manager.build}/bin:$PATH
        export DATA=${dev_data}
      '';

      postInstall = ''
        tar xf ${dev_data}
        make deploy || exit 1;
        make check;

      '';
      shellHook = ''
        runHook preConfigure
        source ${pkgs.bashCompletion}/etc/profile.d/bash_completion.sh
        source $LOGICBLOX_HOME/etc/bash_completion.d/logicblox.sh
      '';
    };

    extraNixPath = {
      src = src_inforecast;
      inherit pdxops measure_query_gen_src;
    };

    webcharonDeployments = "<inforecast/nix/deployments.nix>";

  }
