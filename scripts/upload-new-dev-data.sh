#!/usr/bin/env bash
set -e
set -x

base_url="s3://logicblox-private/platform.predictix.com/test_data/"
pattern="inforecast_dev_data"
new_num="0"

naming_new_data()
{   

	latest_num=$(cloud-store ls ${base_url} | grep ${pattern} | awk -F "${pattern}" '{print $2}' | awk -F ".tgz" '{print $1}' | grep -v "_" | sort -ur | head -1)
	(( new_num=1+${latest_num} ))
}

mkdir -p data-orig
cp -r data/* data-orig
naming_new_data

tar -cvf ${pattern}${new_num}.tgz data/*

echo -e "${brown}--------------------------------"
echo -e "Uploading new dev data set : ${pattern}${new_num}.tgz..."
echo -e "--------------------------------${NC}"
cloud-store upload -i ${pattern}${new_num}.tgz ${base_url}${pattern}${new_num}.tgz --credential-providers-s3 ec2-metadata-service --progress

sha256sum ${pattern}${new_num}.tgz > sha256file.txt
new_sha256=$(cat sha256file.txt | awk -F '  ' '{print $1}')
echo -e "${brown}--------------------------------"
echo -e "Don't forget to update data.nix with new sha256sum : "
echo -e	"${new_sha256}"
echo -e "--------------------------------${NC}"

rm sha256file.txt
rm ${pattern}${new_num}.tgz
rm -rf data
mv data-orig data
rm -rf data-orig