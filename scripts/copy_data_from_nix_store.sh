#!/usr/bin/env bash

set -e

if [ -d "data" ]; then
  rm -r data
fi

tar xzf $DATA