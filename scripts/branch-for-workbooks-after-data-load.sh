#!/usr/bin/env bash

set -e
set -x

if [ `uname` == "Darwin" ]; then
    script_dir=$(cd "$(dirname "$0")"; pwd)
else
    script_dir=$(dirname $(readlink -f $0))
fi
base_dir=$script_dir/..

function create_workbook_base_branch {
    WORKBOOK_TYPE=$1
    shift
    WORKBOOK_BASE_BRANCH_NAME="$BRANCH_NAME"_"$WORKBOOK_TYPE"

    echo "Creating branch for $WORKBOOK_TYPE as $WORKBOOK_BASE_BRANCH_NAME"

    # create the base branch for workbooks
    lb branch $WORKSPACE $WORKBOOK_BASE_BRANCH_NAME --overwrite

    # load services on the branch
    lb web-server load-services -w $WORKSPACE@$WORKBOOK_BASE_BRANCH_NAME

    echo "Load measure rules"
    install_rules $WORKBOOK_TYPE

    # MM: I have deliberatly commented the warmup execution here because It doesn't work since the commands that produce the target run against a different app-prefix than
    # the one needed here. I need someone from IP to explain to me what's the reason we have two scripts to create branches (one before data load and one after).
    # With that said, it's not clear to me how this used to run in the past. I think it never ran!
    # make warmup-views
    # make warmup-rules

    # unload services on branch
    lb web-server unload-services -w $WORKSPACE@$WORKBOOK_BASE_BRANCH_NAME
}

function install_rules {
    template=$1
    RULES_FILES=`find ${base_dir}/src/rules/ -type f -path "*/${template}_*"`
    # ip-workspace will need to become modeler_config:services:prefix:master
    lb measure-service install --uri http://localhost:55183$WORKSPACE/measure -r build/srcgen/rules/*/percent_parent_rules.rules $RULES_FILES
}

#----------------------------------------------------------------------#
# Main
#----------------------------------------------------------------------#

WORKSPACE=$1
BRANCH_NAME=$2
TEMPLATE_LIST="${@:3}"
if [[ -z "$WORKSPACE" || -z "$BRANCH_NAME" || -z "$TEMPLATE_LIST" ]]; then
    echo "USAGE: sh branch_for_workbooks.sh <WORKSPACE> <BASE_BRANCH_PREFIX> <TEMPLATE_LIST>"
    exit 1
fi

lb web-server unload-services -w $WORKSPACE

for template in "$TEMPLATE_LIST"; do
    create_workbook_base_branch "$template"
done

lb web-server load-services -w $WORKSPACE
