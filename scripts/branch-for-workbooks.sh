#!/usr/bin/env bash

set -e
set -x

if [ `uname` == "Darwin" ]; then
    script_dir=$(cd "$(dirname "$0")"; pwd)
else
    script_dir=$(dirname $(readlink -f $0))
fi
base_dir=$script_dir/..

function create_workbook_base_branch {
    WORKBOOK_TYPE=$1
    shift
    WORKBOOK_BASE_BRANCH_NAME="$BRANCH_NAME"_"$WORKBOOK_TYPE"

    echo "Creating branch for $WORKBOOK_TYPE as $WORKBOOK_BASE_BRANCH_NAME"

    # create the base branch for workbooks
    lb branch $WORKSPACE $WORKBOOK_BASE_BRANCH_NAME --overwrite

    # load services on the branch
    lb web-server load-services -w $WORKSPACE@$WORKBOOK_BASE_BRANCH_NAME

    echo "Load measure rules"
    install_rules $WORKBOOK_TYPE

    # MM: Based on comments from Marwa, warmup execution needs to happen at this point (i.e:
    # after branching and running the install_rules function).
    # Note: We should bot have bash scripts that are being executed by make calling into make targets.
    # The reason is that this have the potential of creating circular target dependencies and even when it won't,
    # It makes the build process hard to trace. Therefore, the following 2 lines of code need to be refactored at some point.
    make warmup-views
    make warmup-rules
    
    # unload services on branch
    lb web-server unload-services -w $WORKSPACE@$WORKBOOK_BASE_BRANCH_NAME
}

function install_rules {
    template=$1
    RULES_FILES=`find ${base_dir}/src/rules/ -type f -path "*/${template}_*"`
    # ip-workspace will need to become modeler_config:services:prefix:master
    lb measure-service install --uri http://localhost:55183/ip-workspace/measure -r build/srcgen/rules/*/percent_parent_rules.rules $RULES_FILES

}

#----------------------------------------------------------------------#
# Main
#----------------------------------------------------------------------#

WORKSPACE=$1
BRANCH_NAME=$2
TEMPLATE_LIST="${@:3}"
if [[ -z "$WORKSPACE" || -z "$BRANCH_NAME" || -z "$TEMPLATE_LIST" ]]; then
    echo "USAGE: sh branch_for_workbooks.sh <WORKSPACE> <BASE_BRANCH_PREFIX> <TEMPLATE_LIST>"
    exit 1
fi

lb web-server unload-services -w $WORKSPACE

for template in "$TEMPLATE_LIST"; do
    create_workbook_base_branch "$template"
done

lb web-server load-services -w $WORKSPACE
