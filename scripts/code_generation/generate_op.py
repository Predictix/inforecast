#! /usr/bin/env

import os
import sys
sys.path.insert(0, '%s/python' % os.environ.get('LB_MODELER_HOME'))
import argparse
import clean_csvs
import copy
import csv
import json
import pystache


def clean_csv(measure_file):
    '''
    Takes a csv file and removes C-style comments and empty lines.
    Returns a tuple containing the cleaned CSV as a list of dicts and the fieldnames to preserve header order.
    '''
    with open(measure_file, 'rb') as f:
        clean_f = clean_csvs.remove_blank_lines(clean_csvs.remove_comments(f.read()))
        r = csv.DictReader(clean_f.splitlines())
        return ([row for row in r], r.fieldnames)


def read_csv(measure_list):
    '''
    Reads a csv file.
    '''
    f = open(measure_list, 'r')
    output = f.readlines()
    f.close()
    output = [a.strip() for a in output]
    return output


def find_measures(clean_measures, user_measure_list, version_from):
    '''
    Takes the list of WP measures and the user defined measures.
    Creates two lists: A list of all the WP measures that have _IP in the measure id and the measure list provided by the user.
    '''
    wp_measure_list = []
    measure_list = []
    measure_lib = []
    for row in clean_measures:
        if '_' + version_from in row['Measure'] and '_IP' in row['Measure']:
            wp_measure_list.append(row['Measure'])
        if '_' + version_from in row['Measure'] and '_IP' in row['Measure'] and row['Measure'] in user_measure_list:
            measure_list.append(row['Measure'])
            measure_lib.append(row)
    return wp_measure_list, measure_list, measure_lib


def generate_measures(measure_list, version_from, version_to):
    '''
    Generates a list of OP measures by updating the values from the given list.
    '''
    for measure in measure_list:
        # We also need to update where ever the measure name is used in the config.
        measure.update(Measure=measure['Measure'].replace('_' + version_from, '_' + version_to),
                       RecalcRuleName=measure['RecalcRuleName'].replace('_' + version_from, '_' + version_to),
                       PercentBase=measure['PercentBase'].replace('_' + version_from, '_' + version_to),
                       SpreadByMetric=measure['SpreadByMetric'].replace('_' + version_from, '_' + version_to),
                       Label=measure['Label'].replace('Wp ', version_to + ' '),
                       Readonly='true')
    return measure_list


def validate_measures(to_add, existing):
    '''
    Validates measures before adding them to the existing list.
    Any measures that are found in the list of existing measures are removed from the list of measures to add.
    '''
    # create sets with the measure names
    added_names = set([measure['Measure'] for measure in to_add])
    existing_names = set([measure['Measure'] for measure in existing])
    # remove the measures that already exist from the measures to add
    # the intersection (& operator) is used to find common measures
    valid_names = added_names - (added_names & existing_names)
    return [measure for measure in to_add if measure['Measure'] in valid_names]


def create_list_measures(measures, measures_file):
    '''
    Appends the new measures to the measure file provided (Measures.csv).
    '''
    with open(measures_file, 'ab') as f:
        for measure in measures:
            f.write(measure['Measure'])
            f.write('\n')


def add_measures(cp_measures, fieldnames, measures_file):
    '''
    Appends the new measures to the measure file provided (Measures.csv).
    '''
    with open(measures_file, 'ab') as f:
        f.write('\n')
        w = csv.DictWriter(f, fieldnames=fieldnames, lineterminator='\n')
        w.writerows(cp_measures)


def generate_logic(measures, logic_dir, version_from, version_to):
    '''
    Generates some logic from the provided list of measures using a pystache template.
    '''
    renderer = pystache.Renderer()
    if version_to == 'OP':
        template_path = os.path.join('scripts/code_generation', 'seed_from_OP.mustache')
        logic_path = os.path.join(logic_dir, 'seed_from_OP.logic')
    valid_measures = [measure for measure in measures if not measure['PercentBase'] and
            (measure['Measure'] == 'FirstSlsWk_OP_SCSTQT_IP' or measure['Measure'] == 'NumWksReg_OP_SCSTQT_IP' or measure['Measure'] == 'NumWksMkd_OP_SCSTQT_IP' or measure['Measure'] == 'AdjTktPrice_OP_SCSTQT_IP' or measure['Measure'] == 'SlsReg_AUR_OP_SCSTWK_IP' or measure['Measure'] == 'SlsReg_R_OP_SCSTWK_IP' or measure['Measure'] == 'SlsDisc_AUR_OP_SCSTWK_IP' or measure['Measure'] == 'SlsDisc_R_OP_SCSTWK_IP' or measure['Measure'] == 'RcptTot_C_OP_SCCHWK_IP' or measure['Measure'] == 'RcptTot_R_OP_SCCHWK_IP' or measure['Measure'] == 'RcptTot_AUR_OP_SCCHWK_IP' or measure['Measure'] == 'Cost_OP_SCCHWK_IP')]
            # not measure['RecalcRuleName'] and (measure['Measure'] == 'SlsTot_U_OP_SCCHWK_IP' or measure['Measure'] == 'SlsTot_R_OP_SCCHWK_IP' or measure['Measure'] == 'BOPTot_U_OP_SCCHWK_IP' or measure['Measure'] == 'BOPTot_R_OP_SCCHWK_IP')]
    with open(template_path, 'rb') as template, open(logic_path, 'wb') as logic:
        for measure in valid_measures:
            measure.update(
                Intersection=measure['Intersection'].replace('C', ', c'))
            measure.update(
                Intersection=measure['Intersection'].replace('W', ', w'))
            measure.update(
                Intersection=measure['Intersection'].replace('S', ', s'))
            measure.update(
                Intersection=measure['Intersection'].replace('Q', ', q'))
        rendered_logic = renderer.render(
            template.read(), {'Measures': valid_measures}).split('\n')
        result = []
        for i, line in enumerate(rendered_logic):
            if rendered_logic[i].strip().startswith('^') or line.strip().startswith('!'):
                line = line.replace(
                    '_' + version_to + '_', '_' + version_from + '_')
            result.append(line)
        logic.write('\n'.join(result))


def generate_templates(measures, templates_dir_input, templates_dir_output, template_name):
    '''
    Generates the templates by adding the list of measures under the refresh-all gorup.
    '''
    if measures:
        with open(os.path.join(templates_dir_input, template_name), 'rb') as r:
            data = json.loads(r.read())
            for group in data['refresh_groups']:
                if group['name'] == 'refresh-all':
                    for measure in measures:
                        group['measures'].append(measure['Measure'])

        if not os.path.exists(templates_dir_output):
            os.makedirs(templates_dir_output)
        with open(os.path.join(templates_dir_output, template_name), 'wb') as w:
            w.write(json.dumps(data, indent=2, sort_keys=True))


def main():
    p = argparse.ArgumentParser()
    p.add_argument('-m', '--measures',
                   default='src/config/model/planning/Measures.csv', help='Path to Measures.csv')
    p.add_argument('-o', '--measures_list',
                   help='Path to list of measures')
    p.add_argument('-l', '--logic_dir', default='src/backend',
                   help='Path to the logiql directory')
    p.add_argument('-t', '--templates_dir_input',
                   default='src/config/workbooks/templates', help='Path to workbook templates')
    p.add_argument('--templates_dir_output',
                   default='src/config/workbooks/templates', help='Path to output workbook templates')
    p.add_argument('--version_from', default='WP', help='Source version')
    p.add_argument('--version_to', default='OP', help='Destination version')
    args = p.parse_args()

    template_names = [
        'item_planning_template.json'
    ]

    existing_measures, fieldnames = clean_csv(args.measures)
    existing_measures_copy = copy.deepcopy(existing_measures)
    
    # List of measures provided by the user!
    measures_list = read_csv(args.measures_list)
    #print(measures_list)
    
    #List of measures generated by looking for a specific pattern in the find_measures function!
    wp_measures, gen_measures, gen_measures_lib = find_measures(existing_measures_copy, measures_list, args.version_from)
    #print(gen_measures)

    # Print the diff between the list of measures providedby the user and the generated list!
    diff_measures = [measure for measure in wp_measures if measure not in measures_list]
    diff_measures.sort()
    print("***\nHere is a diff of measures between the list provided and the generated list:\nIf you wish to generate a versioned copy for one of these measures, please add it to the appropriate list under scripts/code_generation!\n***")
    print(diff_measures)

    # Generate, validate, and add the measures from the generated list of measures!
    version_to_measures = generate_measures(gen_measures_lib, args.version_from, args.version_to)
    valid_measures = validate_measures(version_to_measures, existing_measures)
    create_list_measures(valid_measures, 'scripts/code_generation/gen_list_measures.csv')
    add_measures(valid_measures, fieldnames, args.measures)

    # In order to figure out the derivation type of the measure, we are generating the logic at a later stage under scripts/code_generation/genrate_op_logic.py!
    #generate_logic(version_to_measures, args.logic_dir, args.version_from, args.version_to)

    # Commenting out the part that appends the generated measures to templates until we have instantiated templates as part of the build.
    # Else we will need to check in the template with the appended list.
    #for template in template_names:
    #    generate_templates(valid_measures, args.templates_dir_input, args.templates_dir_output, template)

if __name__ == '__main__':
    main()
