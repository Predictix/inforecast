#!/usr/bin/env bash

set -e
set -x

if [ `uname` == "Darwin" ]; then
    script_dir=$(cd "$(dirname "$0")"; pwd)
else
    script_dir=$(dirname $(readlink -f $0))
fi
base_dir=$script_dir/../..

WORKSPACE=$1
WORKBOOK_BASE_BRANCH_NAME=$2

lb web-server unload-services -w $WORKSPACE

### Use the __workbook_base_branch_IP instead of creating a new branch!
# Create a temp branch to install rules.
#lb branch $WORKSPACE find_EDBs --overwrite

#lb web-server load-services -w $WORKSPACE@find_EDBs
lb web-server load-services -w $WORKSPACE@$WORKBOOK_BASE_BRANCH_NAME

# Install Rules
#RULES_FILES=`find ${base_dir}/src/rules/ -type f -path "*/IP_*"`
#lb measure-service install --uri http://localhost:55183/ip-workspace/measure -r build/srcgen/rules/*/percent_parent_rules.rules $RULES_FILES

#make warmup-rules

# Find derivation tpye of WP measures!
sed s/_OP_/_WP_/g scripts/code_generation/gen_list_measures.csv > tmp && mv tmp scripts/code_generation/gen_list_measures.csv

for measure in `cat scripts/code_generation/gen_list_measures.csv`; do
    lb predinfo $WORKSPACE@$WORKBOOK_BASE_BRANCH_NAME "$measure" | grep derivation_type: | sed 's/derivation_type://g' > foo
    for derivation in `cat foo`; do
        if [ "$derivation" == 'DERIVED_AND_STORED' ]; then
            echo $measure is an IDB
            sed /$measure/d scripts/code_generation/gen_list_measures.csv > tmp && mv tmp scripts/code_generation/gen_list_measures.csv
        fi
    done
done

# Return the list of OP measures for code generation!
sed s/_WP_/_OP_/g scripts/code_generation/gen_list_measures.csv > tmp && mv tmp scripts/code_generation/gen_list_measures.csv

#lb web-server unload-services -w $WORKSPACE@find_EDBs
lb web-server unload-services -w $WORKSPACE@$WORKBOOK_BASE_BRANCH_NAME

# Delete the temp branch
#lb delete-branch $WORKSPACE find_EDBs

lb web-server load-services -w $WORKSPACE
