#! /usr/bin/env

import os
import sys
sys.path.insert(0, '%s/python' % os.environ.get('LB_MODELER_HOME'))
import argparse
import clean_csvs
import copy
import csv
import json


def clean_csv(measure_file):
    with open(measure_file, 'rb') as f:
        clean_f = clean_csvs.remove_blank_lines(clean_csvs.remove_comments(f.read()))
        r = csv.DictReader(clean_f.splitlines())
        return ([row for row in r], r.fieldnames)


def validate_measures(to_add, existing):
    # create sets with the measure names
    added_names = set([measure['Measure'] for measure in to_add])
    existing_names = set([measure['Measure'] for measure in existing])
    # remove the measures that already exist from the measures to add
    # the intersection (& operator) is used to find common measures
    valid_names = added_names - (added_names & existing_names)
    return [measure for measure in to_add if measure['Measure'] in valid_names]


def add_measures(cp_measures, fieldnames, measures_file):
    with open(measures_file, 'ab') as f:
        w = csv.DictWriter(f, fieldnames=fieldnames, lineterminator='\n')
        w.writerows(cp_measures)


def main():
    p = argparse.ArgumentParser()
    p.add_argument('-m', '--measures',
                   default='src/config/model/planning/Measures.csv', help='Path to Measures.csv')
    p.add_argument('-o', '--measures_list',
                   help='Path to list of measures')
    args = p.parse_args()

    existing_measures, fieldnames = clean_csv(args.measures)
    measures_list, fieldnames = clean_csv(args.measures_list)

    # Validate and add the measures from the generated list of measures!
    valid_measures = validate_measures(measures_list, existing_measures)
    add_measures(valid_measures, fieldnames, args.measures)

if __name__ == '__main__':
    main()
