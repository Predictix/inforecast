#!/bin/sh
_file_diff="IP_OP_diff.rules"
_file_var="IP_OP_var.rules"
_date=`date`

# This script will generate the required diff and variance rules and copy them under src/rules.

# List of diff measures to add under list_OP_diff_measures.list
# List of var measures to add under list_OP_var_measures.list
# Declare both diff and var measures under list_OP_measures_diff_var.csv

# Diff Measures!
echo "//////////////////////////////" > ${_file_diff}
echo "// Generated Code" >> ${_file_diff}
echo "// (${_date}) " >> ${_file_diff}
echo "//////////////////////////////" >> ${_file_diff}

for i in `cat scripts/code_generation/list_OP_diff_measures.list | tail -n +2`
do
    diff_measure=`echo $i| awk -F\| '{print $1}'`
    WP_measure=`echo $i| awk -F\| '{print $2}'`
    OP_measure=`echo $i| awk -F\| '{print $3}'`
    echo "rule \"${diff_measure}\" {" >> ${_file_diff}
    sed -e "s|diff_measure|$diff_measure|g" -e "s|WP_measure|$WP_measure|g" -e "s|OP_measure|$OP_measure|g" scripts/code_generation/OP_diff_Rules.template >> ${_file_diff}
    echo "}" >> ${_file_diff}
done

cp -v ${_file_diff} src/rules/.

# Variance Measures!
echo "//////////////////////////////" > ${_file_var}
echo "// Generated Code" >> ${_file_var}
echo "// (${_date}) " >> ${_file_var}
echo "//////////////////////////////" >> ${_file_var}

for i in `cat scripts/code_generation/list_OP_var_measures.list | tail -n +2`
do
    var_measure=`echo $i| awk -F\| '{print $1}'`
    WP_measure=`echo $i| awk -F\| '{print $2}'`
    OP_measure=`echo $i| awk -F\| '{print $3}'`
    echo "rule \"${var_measure}\" {" >> ${_file_var}
    sed -e "s|var_measure|$var_measure|g" -e "s|WP_measure|$WP_measure|g" -e "s|OP_measure|$OP_measure|g" scripts/code_generation/OP_var_Rules.template >> ${_file_var}
    echo "}" >> ${_file_var}
done

cp -v ${_file_var} src/rules/.
