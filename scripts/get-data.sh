#!/usr/bin/env bash
set -e
loc=$1

tar xf ${loc}
pushd data > /dev/null
find . -name '*.gz' | xargs gunzip
popd > /dev/null