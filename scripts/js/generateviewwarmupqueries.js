/* eslint-env node */

"use strict";

var fs = require("fs");
var path = require("path");
var parseArgs = require("minimist");

var ModelerTestUtil = require("logicbloxtestutils").ModelerTestUtil;

/**
 * Script to warmup views in an application.
 * Assumes canvases sheets are in src/config/canvas. Another one can be passed
 * in using the --config-dir parameter.
 *
 * Mandatory paramters are --app-prefix
 *
 * Optional parameters:
 * - server-url (default http://localhost:55183)
 * - config-dir (default src/config/canvas)
 * - canvas (id of a canvas if we want to warmup just one canvas)
 */

if (require.main !== module) {
    throw new Error("This module should run from console");
}

var argv = parseArgs(process.argv.slice(2), {
        default: {
            "config-dir": "./src/config/canvas",
            "server-url": "http://localhost:55183",
        },
    }),
    serverUrl = argv["server-url"],
    canvasesPath = argv["config-dir"],
    appPrefix = argv["app-prefix"],
    saveQueriesTo = argv._[0],
    modelerTestUtil = new ModelerTestUtil({
        appPrefix: serverUrl + "/" + appPrefix,
        workspace: "foo" // ModelerTestUtil requires workspace but we don't need it for warmups
    }),
    modelerInitializedPromise = initializeModeler(modelerTestUtil),
    canvasIdsPromise = findCanvasIds(canvasesPath);

Promise.all([
    modelerInitializedPromise, canvasIdsPromise
]).then(function(results) {
    return generateWarmupQueries(results[1]);
}).then(function(warmupQueries) {
    return saveQuery(saveQueriesTo, warmupQueries);
}).then(function() {
    process.exit();
}).catch(function(err) {
    console.error(err, err.stack);
    process.exit(1);
});

function findCanvases(canvasesPath) {
    return listFilesIn(canvasesPath).then(function(fileNames) {
        return fileNames.filter(function(filePath) {
            var isJson = filePath.endsWith(".json"),
                isTemplate = filePath.indexOf("/templates/") !== -1;

            return isJson && !isTemplate;
        });
    });
}

function initializeModeler(modelerTestUtil) {
    return modelerTestUtil.modelerApp.getModelerActionHandler().initializeModeler();
}

function generateWarmupQueries(canvasIds) {
    return modelerTestUtil.generateWarmupQueries(canvasIds).then(function(query) {
        return {
            canvasIds: canvasIds,
            query: query
        };
    }).catch(function(err) {
        console.error("Error generating queries for " + canvasIds + ". Look into server log for more details");

        throw err;
    });
}


/**
 * Builds a promise for reading files
 *
 * @param  {String} filePath
 * @return {Promise}
 */
function readFile(filePath) {
    return new Promise(function(resolve, reject) {
        fs.readFile(filePath, function(err, data) {
            if (err) {
                reject(err);
            } else {
                resolve({
                    filename: filePath,
                    data: JSON.parse(data)
                });
            }
        });
    });
}


function findCanvasIds(canvasesPath) {
    return findCanvases(canvasesPath).then(function(canvasFiles) {
        return Promise.all(canvasFiles.map(readFile)).then(function(canvases) {
            return canvases.map(function(canvasFile) {
                var canvas = canvasFile.data;

                if (!canvas.id) {
                    console.warn("Could not find id for " + canvasFile.filename);
                }

                return canvas.id;
            }).filter(function(q) { return q; });
        });
    });
}


function listFilesIn(dir) {
    return new Promise(function(resolve, reject) {
        walk(dir, function(err, result) {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
    });
}

function saveQuery(saveQueriesTo, warmupQuery) {
    console.log("Saving warmup queries for canvases to " + saveQueriesTo);

    var exprs = warmupQuery.query.install_request.measure_expr,
        query = {};

    // We generate an empty request if we don't have any query expressions
    // to avoid getting 500 errors from measure service since measure expressions
    // are mandatory for install_requests
    if (exprs.length > 0) {
        query = {
            install_request: {
                measure_expr: exprs
            }
        };
    }

    var filename = path.join(saveQueriesTo, "view-queries.json");

    return saveRequestAsync(filename, query);
}

function saveRequestAsync(filename, warmupQuery) {
    return new Promise(function(resolve, reject) {
        fs.writeFile(filename, JSON.stringify(warmupQuery), function(err) {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
}

/**
 * Synchronous function for recursively listing files in a directory
 *
 * @param  {String}   dir   path of the directory to read from
 * @param  {Function} done  callback to execute when finished
 */
function walk(dir, done) {
    var results = [];

    fs.readdir(dir, function(err, list) {
        if (err) {
            done(err);

            return;
        }

        var pending = list.length;

        if (!pending) {
            done(null, results);

            return;
        }

        list.forEach(function(file) {
            file = path.resolve(dir, file);

            fs.stat(file, function(err, stat) {
                if (stat && stat.isDirectory()) {
                    walk(file, function(err, res) {
                        results = results.concat(res);

                        if (!--pending) {
                            done(null, results);
                        }
                    });
                } else {
                    results.push(file);

                    if (!--pending) {
                        done(null, results);
                    }
                }
            });
        });
    });
}
