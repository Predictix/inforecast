#!/usr/bin/env python

import os 
from sets import Set
import sys 
import argparse
import ConfigParser
import timeit
import time
import subprocess
from datetime import date
import time
import subprocess


def run_command(command):
    time_total = 0.0
    datetime_format = '%m/%d/%y %H:%M'
    time_start = timeit.default_timer()
    ret_code = subprocess.call(command, shell=True, cwd=os.curdir)

    time_delta = (timeit.default_timer() - time_start)/60.0
    time_total += time_delta
    return ret_code


def convert_netmap(args):
    import json
    import os.path
    from shutil import copyfile

    if os.path.isfile("/etc/logicblox/netmap_info.csv"):
        copyfile("/etc/logicblox/netmap_info.csv", "/tmp/netmap_info.csv")
    else:
        copyfile("%s/netmap_info.csv"%args.config_dir, "/tmp/netmap_info.csv")

    
def _set_workflow_parameters(args):
    command="lb exec /workflow '^target_dir[]=\"%s\".'"%(args.target_dir)
    ret_code=run_command(command)
    command="lb exec /workflow '^network_path[]=\"%s\".'"%(args.network_path)
    ret_code=run_command(command)
    command="lb exec /workflow '^workspaces[]=\"%s\".'"%(args.workspaces)
    ret_code=run_command(command)
    command="lb exec /workflow '^config_dir[]=\"%s\".'"%(args.config_dir)
    ret_code=run_command(command)
    command="lb exec /workflow '^extra_config_dir[]=\"%s\".'"%(args.extra_config_dir)
    ret_code=run_command(command)
    

    
def _bootstrap(args):
    _set_workflow_parameters(args)

    command="lb exec /workflow '^gen_zero[]=\"%s\".'"%(args.gen_zero)
    ret_code=run_command(command)

    command="lb exec /workflow '^data_dir[]=\"%s\".'"%(args.data_dir)
    ret_code=run_command(command)

    command="lb workflow start --workspace /workflow --name bootstrap_nm"
    ret_code=run_command(command)

def _fm_import(args):
    _set_workflow_parameters(args)
    command="lb workflow start --workspace /workflow --name dev_deploy"
    ret_code=run_command(command)


def generic_parser(subparsers, cmd, help_str):
    current_date=time.strftime("%Y%m%d_%H%M%S")
    p = subparsers.add_parser(cmd, help=help_str)
    p.add_argument('--workspaces_path', '-p', required=False, default='/data/lb_deployment', help='Local workspaces path')
    
    p.add_argument('--target_dir', '-t', required=False, default="/data/lb_deployment/installed-app" , help='[OPTIONAL] target directory that contains the \
        closure, if not provided /data/lb_deployment/installed-app will be used')
    p.add_argument('--network_path', '-n', required=False, default="/data/lb_deployment/installed-app/network-manager", help='[OPTIONAL] network manager path, \
        , if not provided /data/lb_deployment/installed-app/network-manager will be used')
    p.add_argument('--workspaces', '-w', required=False, default="/data/lb_deployment/uncompressed-workspaces", help='[OPTIONAL] network manager path, \
        , if not provided /data/lb_deployment/uncompressed-workspaces will be used')
    p.add_argument('--config_dir', '-c', required=False, default="/data/lb_deployment/installed-app/config", help='[OPTIONAL] config directory, \
        , if not provided /data/lb_deployment/installed-app/config will be used')
    p.add_argument('--extra_config_dir', '-ec', required=False, default="/tmp", help='[OPTIONAL] extr config directory containing information about the current cluster, \
        , if not provided /tmp will be used')
    return p

def main():
    
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(help='Command help')

    p = generic_parser(subparsers, 'bootstrap', 'Deploy /network workspace the network manager workspace and current generation staging')
    # gen_zero is required and will be used to set the generation starting point in the network manger workspace
    p.add_argument('--gen_zero', '-g', required=True, help='generation zero id')
    p.add_argument('--data_dir', '-d', required=True, help='data path prefix')
    p.set_defaults(func=_bootstrap)


    p = generic_parser(subparsers, 'fm_import', 'Deploy FM and import data')
    p.set_defaults(func=_fm_import)

    args = parser.parse_args()
    
    convert_netmap(args)
    args.func(args)

if __name__ == '__main__':
    main()  