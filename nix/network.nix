{ logLevel ? "info"
, bucket ? "inforecast-dev"
, manual_current_date ? ""
, genZero ? "20160803"
, extraUsers ? [ "chaima.mejri"]
, netmap ? null
, installFlags ? "--frontend"
,dataLoc ? "inforecast-data/"
, ...
}:
{
  require = [ <lbdevops/nixops/partitioned/network.nix> ];
  defaults = { config, lib, ... }:
  { 
    imports = [ <lbdevops/nixos/base.nix> ];
    users.lbAdminUsers = extraUsers;
    services.logicblox.config.lb-server = ''
      [logging]
      level = ${logLevel}
    '';
    logicblox.application.installFlags = installFlags;
    services.logicblox.config.lb-workflow-driver = ''
    extension_dirs = /data/lb_deployment/installed-app/network-manager/
  '';
  };

  frontend = { config, lib, pkgs, ... }:
  {
    environment.systemPackages = [
	pkgs.nodejs
	pkgs.pythonPackages.numpy
	pkgs.pythonPackages.scipy
	pkgs.pythonPackages.pandas
   ];
    environment.shellInit = ''
      export MEASURE_QUERY_GEN_VIEWS=/data/lb_deployment/installed-app/measure_views/gridsConfig.js
      export MEASURE_QUERY_GEN_HOME=${(builtins.storePath <measure_query_gen_src>)}
    '';
    services.logicblox.lbWorkflowWorkspaces = [ "/workflow" ];
    systemd.services.bootstrap = {
      description = "bootstrap";
      wantedBy = [ "multi-user.target" ];
      requires = [ "install-app.service" ];
      after = [ "install-app.service" ];
      script = "source /etc/profile; python /data/lb_deployment/installed-app/wf_wrapper.py bootstrap -g ${genZero} -d s3://${bucket}/${dataLoc}";
      serviceConfig = {
        User = "logicblox";
        Type = "oneshot";
        Group = "logicblox";
        RemainAfterExit = true;
      };
    };
    systemd.services.import-fm = {
      description = "bootstrap";
      wantedBy = [ "multi-user.target" ];
      requires = [ "bootstrap.service" ];
      after = [ "bootstrap.service" ];
      script = "source /etc/profile; python /data/lb_deployment/installed-app/wf_wrapper.py fm_import";
      serviceConfig = {
        User = "logicblox";
        Type = "oneshot";
        Group = "logicblox";
        RemainAfterExit = true;
      };
    };
  

    environment.etc."logicblox/netmap.csv" =
      let
        netmap' = builtins.fromJSON (builtins.readFile netmap);
        netmap_csv =
            "HOSTNAME|PARTITION\n"
          + lib.concatStrings (lib.flatten (map (n: map (ops: "${n}|${ops}\n") netmap'."${n}" )(builtins.attrNames netmap')));
      in
        lib.mkIf (netmap != null) { text = lib.mkOverride 0 netmap_csv; };
  };

}
