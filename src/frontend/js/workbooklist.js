"use strict";

var Table = FixedDataTable.Table;
var Column = FixedDataTable.Column;

var SortTypes = {
    ASC: 'ASC',
    DESC: 'DESC',
};

var WorkbooksTable = React.createClass({
    propTypes: {
        workbookList: React.PropTypes.array,
        tableWidth: React.PropTypes.number,
        tableHeight: React.PropTypes.number
    },
    getInitialState: function() {
        return {
            filterBy: null,
            filteredList: this.props.workbookList,
            sortBy: 'name',
            sortDir: null
        };
    },
    getDefaultProps: function() {
        return {
            workbookList: []
        };
    },
    renderLink: function(link) {
        return link;
    },
    _onContentHeightChange: function(contentHeight) {
        this.props.onContentDimensionsChange &&
            this.props.onContentDimensionsChange(
                contentHeight,
                Math.max(600, this.props.tableWidth)
            );
    },
    getTableRows: function() {
        return this.state.filteredList.filter(isNotDeleted).map(function(workbookMsg) {
            var tmpl = getTemplateFromWorkbookName(workbookMsg.name),
                html = getHtmlFileForTemplate(tmpl),
                workbook_name = workbookMsg.name || tmpl + " workbook - " + workbookMsg.id,
                appnode = workbookMsg.appnode,
                params = [];

            if (workbookMsg.id) {
                params.push('workbook_id=' + workbookMsg.id);
            }

            if (appnode) {
                params.push('appnode='+ appnode);
            }

            return {
                name: workbook_name,
                link: React.DOM.a({
                    href: html + "?" + params.join("&"),
                    target: "_blank"
                }, workbook_name),
                start_build_time: workbookMsg.start_build_time.split(",")[0]
            };
        })
    },
    onFilterChange: function(e) {
        this.filterRowsBy(e.target.value);
    },
    filterRowsBy: function(filterBy) {

        var rows = this.props.workbookList.slice();
        var filteredRows = filterBy ? rows.filter(function(row) {
            return row.name.toLowerCase().indexOf(filterBy.toLowerCase()) >= 0
        }) : rows;
        this.setState({
            filteredList: filteredRows,
            filterBy: filterBy
        })

    },
    sortRowsBy: function(cellDataKey) {
        var sortDir = this.state.sortDir;
        var sortBy = cellDataKey;
        if (sortBy === this.state.sortBy) {
            sortDir = this.state.sortDir === SortTypes.ASC ? SortTypes.DESC : SortTypes.ASC;
        } else {
            sortDir = SortTypes.DESC;
        }

        var rows = this.state.filteredList.slice();
        rows.sort(function(a, b) {
            var sortVal = 0;
            if (a[sortBy] > b[sortBy]) {
                sortVal = 1;
            }
            if (a[sortBy] < b[sortBy]) {
                sortVal = -1;
            }

            if (sortDir === SortTypes.DESC) {
                sortVal = sortVal * -1;
            }

            return sortVal;
        });

        this.setState({
            filteredList: rows,
            sortBy: sortBy,
            sortDir: sortDir,
        });
    },

    renderHeader: function(label, cellDataKey) {
        return React.DOM.a({
            className: "workbookTableHeader",
            onClick: this.sortRowsBy.bind(null, cellDataKey === 'link' ? 'name' : cellDataKey),
        }, label);
    },
    render: function() {
        var rows = this.getTableRows(),
            sortBy = this.state.sortBy,
            sortDirArrow = '';

        if (this.state.sortDir !== null) {
            sortDirArrow = this.state.sortDir === SortTypes.DESC ? ' ↓' : ' ↑';
        }

        function rowGetter(rowIndex) {
            return rows[rowIndex];
        }

        function labelGetter(label, key) {
            return label + (sortBy === key ? sortDirArrow : '');
        }

        return React.DOM.div({},
            React.DOM.input({
                className: "form-control workbookTableFilter",
                onChange: this.onFilterChange,
                placeholder: 'Filter by workbook name'
            }),
            React.createElement(Table, {
                    width: this.props.tableWidth,
                    height: this.props.tableHeight,
                    onContentHeightChange: this._onContentHeightChange,
                    rowHeight: 50,
                    rowGetter: rowGetter,
                    rowsCount: rows.length,
                    headerHeight: 50
                },
                React.createElement(Column, {
                    headerRenderer: this.renderHeader,
                    cellRenderer: this.renderLink,
                    label: labelGetter("Workbook Name", "name"),
                    width: this.props.tableWidth / 2,
                    dataKey: 'link'
                }),
                React.createElement(Column, {
                    headerRenderer: this.renderHeader,
                    label: labelGetter("Creation Date", "start_build_time"),
                    width: this.props.tableWidth / 2,
                    dataKey: 'start_build_time'
                }))
        );
    }
});

var WorkbooksTableController = React.createClass({
    propTypes: {
        workbookList: React.PropTypes.array,
        targetContainer: React.PropTypes.object
    },
    getInitialState: function() {
        return {
            height: 0,
            width: 0
        };
    },
    render: function() {
        var props = {
            workbookList: this.props.workbookList,
            tableWidth: this.state.width,
            tableHeight: this.state.height
        };

        return React.createElement(WorkbooksTable, props);
    },
    updateDimensions: function() {
        var contentClientRect = this.props.targetContainer.getBoundingClientRect();

        this.setState({
            width: contentClientRect.width,
            height: contentClientRect.height
        });
    },
    componentWillMount: function() {
        this.updateDimensions();
    },
    componentDidMount: function() {
        window.addEventListener("resize", this.updateDimensions);
    },
    componentWillUnmount: function() {
        window.removeEventListener("resize", this.updateDimensions);
    }

})

function getTemplateFromWorkbookName(name) {
    if (name.indexOf("Global Manager") > -1)
        return "globalmanager";
    else
        throw new Error("Unable to parse template for workbook name: " + name);
}

function getHtmlFileForTemplate(template) {
    switch (template) {
        case "globalmanager":
            return "global-manager.html";
        default:
            throw new Error("Unable to find HTML file for template: " + template);
    }
}

function isNotDeleted(workbook) {
    return !workbook.hasOwnProperty("deleted_on");
}
