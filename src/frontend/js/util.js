/* globals logicblox, jQuery, React */
"use strict";

var util = logicblox.util;

function parseURL(url) {
    var parser = document.createElement('a'),
        searchObject = {},
        queries, split, i;
    // Let the browser do the work
    parser.href = url;
    // Convert query string to object
    queries = parser.search.replace(/^\?/, '').split('&');
    for (i = 0; i < queries.length; i++) {
        split = queries[i].split('=');
        searchObject[split[0]] = split[1];
    }
    return {
        protocol: parser.protocol,
        host: parser.host,
        hostname: parser.hostname,
        port: parser.port,
        pathname: parser.pathname,
        search: parser.search,
        searchObject: searchObject,
        hash: parser.hash
    };
}

function syncCall(method, url, data, resolve, reject) {
    jQuery.ajax({
        type: method,
        url: url,
        data: data,
        dataType: "json",
        async: false,
        headers: {
            "ContentType": "application/json",
            "Accept": "application/json"
        },
        success: function(result) {
            resolve(result);
        },
        error: function(err) {
            err.url = url;
            reject(err);
        }
    });
}
