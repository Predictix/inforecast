"use strict";

var Modeler = logicblox.Modeler;
var contentEl = document.getElementById("content");

// OPTIONS
var LOGIN_URL = "/login",
    REALM = "ip-workspace-realm",
    broadcastUrlPrefix = "/ui";
/*************************************
* MM: Defaults
*************************************/
var app_prefix = "";
var WORKSPACE = app_prefix;
/*************************************
* MM: Actual values from /ui/appnodes service
*************************************/
syncCall("POST", "/ui/appnodes", {}, function(response) {
    var appnodes = response.appnode;
    // MM: Referencing the first element in the response since we only have one node for now.
    // This needs to change when the modeler starts supporting multiple partitions.
    app_prefix = "/ui" + appnodes[0].id;
    WORKSPACE = app_prefix;
}, function(error){
    console.log("Something went wrong!");
});

var props = {
    workspace: WORKSPACE,
    appPrefix: app_prefix,
    broadcastUrlPrefix: broadcastUrlPrefix,
    enableUserBroadcast: true,
    displayWorkbookList: true,
    navigationTreeId: "item_planning_template",
    realm: REALM,
    loginUrl: LOGIN_URL,
    urls: {
        currentUserUrl: "/ui/admin/current_user"
    },
    brandImg: React.DOM.img({ alt: "inforecast Item Planning", src: "/images/inforecast.png"}),
    modelingFeatures: {
        editSchema: false,
        editRules: false,
        collaboration: {
            minUpdateInterval: 0,
            maxUpdateInterval: 6
        },
        idleTimeValues: {
            idleNotificationTime: 3480,
            ignoreBroadcastingTime: 600,
            idleLogoutTime: 120
        }
    },
    actionButtons: [{
            actionId: "commit-all", label: "Commit"
        }, {
            actionId: "refresh-all", label: "Refresh"
    }]
};

/**
 * Setting IP up with SSO only when the application is deployed to a remote server
 * Locally, username/password login is used
 */
if (window.location.hostname !== "localhost" && window.location.hostname !== "127.0.0.1") {
    props.singleSignOn = {
        requestUrl: window.location.origin + "/sso/request",
        appLogo: "/images/inforecast.png",
        appName: "inforecast",
        predictixLogo: "/images/favicon.ico"
    };
}

var modelerView = React.createElement(Modeler, props);

ReactDOM.render(modelerView, contentEl);
