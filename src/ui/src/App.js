import React, { Component }  from 'react';
import 'IR-style-guide-react-bootstrap/css/bootstrap.css';
import 'IR-style-guide-react-bootstrap/css/bootstrap-theme.css';
import 'IR-style-guide/styleGuide-bootstrap-mediaQuery.css';
import './Style/App.scss';
import { Router, Route, IndexRoute, hashHistory } from 'react-router';


import MasterPage from './components/pages/masterPage/MasterPage';
import Search from './components/pages/search/Search';
import ResultByProducts from './components/pages/resultByProducts/ResultByProducts';

class App extends Component {

  render() {
    return (
      <Router history={hashHistory}>
         <Route path="/" component={MasterPage}>
            <IndexRoute component={Search} />
            <Route path="/ResultByProducts" component={ResultByProducts} />
         </Route>
      </Router>

    );
  }
}

export default App;
