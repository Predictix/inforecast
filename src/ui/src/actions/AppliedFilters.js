
export function updateSkus(skus) {
   return function(dispatch) {
    	dispatch({type: "ADD_SKU_FULFILLED", payload: {skus}})
   }
}

export function updateStores(stores) {
   return function(dispatch) {
    	dispatch({type: "ADD_STORE_FULFILLED", payload: {stores}})
    }
}

