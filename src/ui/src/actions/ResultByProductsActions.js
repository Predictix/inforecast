
import MQG from './MQG';
var gridsconfig = require ('../../measure_views/gridsConfig');
var mqg = new MQG('/measure', gridsconfig);

export function fetchProducts(query) {
  return function(dispatch) {
    mqg.query('resultByProduct' + query)
      .then((response) => {
        dispatch({type: "RESULT_BY_PRODUCTS_FULFILLED", payload: response})
      })
      .catch((err) => {
        dispatch({type: "RESULT_BY_PRODUCTS_REJECTED", payload: err})
      })
  }
}
