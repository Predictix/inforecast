import MQGLib from 'measure-query-gen/src/MQU';

class MQG {
    constructor(measureServiceUrl = '/measure', relaxConfig = function(){}) {
        this.mqg = null;
        this.initialize = this._memoize(this.initialize);
        this.query = this._ensureInitialized(this.query);
        this.getMetaModel = this._ensureInitialized(this.getMetaModel);
        this.measureServiceUrl = measureServiceUrl;
        this.relaxConfig = relaxConfig;
    }

    initialize() {
      var that = this
        return new Promise((resolve, reject) => {
            MQGLib.fetchMetaModel(this.measureServiceUrl).then(metaModel => {
                const config = {
                    measureService: this.measureServiceUrl,
                    metaModel: metaModel
                };

                that.mqg = new MQGLib(config);
                that.mqg.setLogLevel('INFO');
                that.relaxConfig(this.mqg);

                resolve();
            }, error => {
                reject(error);
            });
        });
    }

    query(uri) {
        const request = new this.mqg.RelaxRequest();
        request.push(uri);

        return new Promise((resolve, reject) => {
            request.getResponse().then(
                data => resolve(data[0]),
                error => {
                    reject(error);
                });
        });
    }

    getMetaModel() {
        return this.mqg.metaModel.metaModel.model;
    }


    _memoize(f) {
        let result;

        return function() {
            if (!result) {
                result = f.apply(this, arguments);
            }

            return result;
        }
    }

    _ensureInitialized(f) {
        return function() {
            const args = arguments;
            return this.initialize().then(() => f.apply(this, args));
        }
    }
}

export default MQG;
