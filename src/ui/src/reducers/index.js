import { combineReducers } from 'redux';

import resultByProduct from './resultByProductsReducer';
import filters from './appliedFilters';

export default combineReducers({
  resultByProduct,
  filters
})
