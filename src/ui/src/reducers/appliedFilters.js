export default function reducer(state={
    skus: [],
    stores: [],
    fetching: false,
    fetched: false,
    error: null,
  }, action) {

    switch (action.type) {
      case "ADD_SKU": {
        return {...state, fetching: true}
      }
      case "ADD_SKU_FULFILLED": {
        return {
          ...state,
          fetching: false,
          fetched: true,
          skus: action.payload.skus
        }
      }
      case "ADD_STORE": {
        return {...state, fetching: true}
      }
      case "ADD_STORE_FULFILLED": {
        return {
          ...state,
          fetching: false,
          fetched: true,
          stores: action.payload.stores
        }
      }
      default : {
        return {
          ...state,
          fetching: false,
          fetched: false,
          skus: [],
          stores: []
        }
      }
    }
}