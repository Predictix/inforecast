export default function reducer(state={
    resultByProducts: [],
    fetching: false,
    fetched: false,
    error: null,
  }, action) {

    switch (action.type) {
      case "FETCH_RESULT_BY_PRODUCTS": {
        return {...state, fetching: true}
      }
      case "FETCH_RESULT_BY_PRODUCTS_REJECTED": {
        return {...state, fetching: false, error: action.payload}
      }
      case "RESULT_BY_PRODUCTS_FULFILLED": {
        return {
          ...state,
          fetching: false,
          fetched: true,
          resultByProducts: action.payload,
        }
      }
      default : {
        return {
          ...state,
          fetching: false,
          fetched: false,
          resultByProducts: [],
        }
      }
    }
}
