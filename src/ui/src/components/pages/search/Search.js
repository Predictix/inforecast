import React , { Component } from "react";
import ReactDOM from 'react-dom';
import { connect } from "react-redux";
import { Button, IconLibrary, Icon, Stereo } from 'IR-style-guide';
import { updateSkus, updateStores} from '../../../actions/AppliedFilters.js';
import store from '../../../Store';
import InputFilter  from './inputFilter/InputFilter';
import SelectedFilter from './selectedFilter/SelectedFilter';
import ViewTitle from '../../ViewTitle';
import GroupCard from '../../groupCard/GroupCard';

import './Search.scss';



class Search extends  Component {
    
    addFilter(e){
      let skus = this.props.skus;
      let stores = this.props.stores;

      const filter = e.criteria.filter;
      const val = e.filtervalue;
        switch (filter) {
          case "sku":
            if( skus.indexOf(val) < 0){
              skus.push(val);
              store.dispatch(updateSkus(skus));
              this.renderFilters ("SKU", this.props.skus);
            }
            break;
          case "store":
            if( stores.indexOf(val) < 0){
              stores.push(val);
              store.dispatch(updateStores(stores));
              this.renderFilters ("STORE", this.props.stores);
            }
            break;
          default :{}

          } 
    }

    renderFilters (filterName, list){

        let filterList=list.length ?
          <GroupCard 
            title={filterName}
            groupCardTitleBLU
            content={list.map(
                (sku, index) => 
                  <SelectedFilter 
                    key={index}
                    filterValue={sku}
                    filterName={filterName} 
                    clearState={this.clearState.bind(this)}
                  />
                )
              }
            />
          :
          <span></span>;

       ReactDOM.render(filterList,document.getElementById(filterName))
    }

    clearState (e){
      let skus = this.props.skus;
      let stores = this.props.stores;
      const filter = e.filterName;
      const val = e.filterValue;
        switch (filter) {
          case "SKU":{
            let index = skus.indexOf(val);
            if( index > -1){
              skus.splice(index, 1);
              store.dispatch(updateSkus(skus));  
              this.renderFilters ("SKU", this.props.skus);
            }            
              break;
          }
          case "STORE":{
            let index = stores.indexOf(val);
            if( index > -1){
              stores.splice(index, 1);
              store.dispatch(updateStores(stores));
              this.renderFilters ("STORE", this.props.stores);              
            }
            break;
          }
          default:{}

          }    
    }
    
    handleSearch(){
      this.props.router.push({
        pathname: '/ResultByProducts',
        query: {
          selectedSku:String(this.props.skus[0]),
          skus: String(this.props.skus),
          stores: String(this.props.stores),
          Productlevel: 'sku',
          Locationlevel: 'store',
          Calendarlevel: 'week'
        }
      })
    }

  render(){

    let searchCriteria = { sku:{
      label: 'SKU',
      filter: 'sku'
    },store:{
      label: 'STORE',
      filter: 'store'
    }}

    return (
    <div className="search-view-container">
       <ViewTitle title="Search"/>
       <Stereo className="search-view-stereo">
          <Stereo.Side  orientation={Stereo.Side.ORIENTATION.LEFT} className="search-view-stereo-left-side">
            <div className="search-view-appliedFilters-section">
              <article className="search-view-appliedFilters-header">Applied Filters</article>
               <div id="SKU">
               </div>
               <div id="STORE">
               </div>
            </div>
            <div className="search-view-searchButton-section">
            <Button  presentation={Button.PRESENTATION.PRIMARY} onClick={this.handleSearch.bind(this)} icon={
            <IconLibrary.SearchIcon size={Icon.SIZE.SMALL} />
            } >
            Search
            </Button>
            </div>
          </Stereo.Side>
          <Stereo.Side  orientation={Stereo.Side.ORIENTATION.RIGHT} className="search-view-stereo-right-side">
            <div className="search-view-filterList-section">
               <div className="search-panel">
                <GroupCard 
                  title={"Product"}
                  groupCardTitleGRAY
                  content={
                    <span>
                     <InputFilter 
                      criteria={searchCriteria.sku} 
                      addFilter={this.addFilter.bind(this)}/>

                      <InputFilter 
                      criteria={searchCriteria.sku} 
                      addFilter={this.addFilter.bind(this)}/>
                    </span>
                    } 
                  />
               </div>

               <div className="search-panel">
                  <GroupCard 
                    title={"Location"}
                    groupCardTitleGRAY
                    content={
                      <span>
                       <InputFilter 
                        criteria={searchCriteria.store} 
                        addFilter={this.addFilter.bind(this)}/>

                        <InputFilter 
                        criteria={searchCriteria.sku} 
                        addFilter={this.addFilter.bind(this)}/>
                      </span>
                      } 
                    />
               </div>
            </div>
          </Stereo.Side>
       </Stereo>
    </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    skus: state.filters.skus,
    stores: state.filters.stores
  };
}

export default  connect(mapStateToProps) (Search);
