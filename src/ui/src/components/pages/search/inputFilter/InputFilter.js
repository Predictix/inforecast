import React , { Component } from "react";
import { Button, IconLibrary, Icon } from 'IR-style-guide';


import './InputFilter.scss'

export default class InputFilter extends  Component {
  
  constructor() {
    super();
    this.state = this.initState();
  };

  initState () {
    return {
      filterValue: '',
      isDisabled: true  
    }
  };
  handleInputChange (e) {
    let isDisabled = (e.target.value.length === 0) ? true : false;
    this.setState({
      filterValue: e.target.value,
      isDisabled: isDisabled
    });
  };

  handleOnClick (){
   if(this.refs.filterValue.value){
     let addFilter_input = {
      filtervalue : this.state.filterValue,
      criteria: this.props.criteria
     }
     this.props.addFilter(addFilter_input);
     this.refs.filterValue.value = '';
     this.setState (this.initState);
   }
  };

  render(){
    return (
      <div className="filter"> 
        <label className="inforecast-search-label">{this.props.criteria.label}</label>
        <input onChange={(e) => {this.handleInputChange(e)}} className="inforecast-textField" id="mytext" ref="filterValue" ></input>
        <Button presentation="Primary" isDisabled={this.state.isDisabled} onClick={this.handleOnClick.bind(this)} icon={<IconLibrary.AddIcon size={Icon.SIZE.SMALL} />}>
        </Button>
      </div>
    )
  }
}
