import React , { Component } from "react"; 
import { IconLibrary, Icon, Button} from 'IR-style-guide';


import './SelectedFilter.scss';

export default class SelectedFilter extends  Component {  


  handleOnClick (){
    let clearState_input = {
      filterName: this.props.filterName,
      filterValue: this.props.filterValue
    }
    this.props.clearState(clearState_input);

  }

  render(){
      return (
        <span className="selected-filter">
          <Button onClick={this.handleOnClick.bind(this)} presentation={Button.PRESENTATION.SECONDARY} icon={<IconLibrary.DeleteIcon size={Icon.SIZE.SMALL} />} >
            {this.props.filterValue}
          </Button>
        </span>
    )

  }
}


