import React, { Component } from 'react';

import './MasterPage.scss';
import Header from '../../Header';


export default class MasterPage extends  Component {
	render(){
		return (
			<div className="master-page">
			   <Header />
			   <div className="main">
			   {this.props.children}
			   </div>
			</div>
	    )  
	}
}
