import React , { Component } from 'react';
import { AgGridReact } from 'ag-grid-react';
import { fetchProducts } from '../../../actions/ResultByProductsActions.js';
import { connect } from "react-redux";

import '../../../../node_modules/ag-grid/dist/styles/ag-grid.css';
import '../../../../node_modules/ag-grid/dist/styles/theme-fresh.css';
import './ResultByProducts.scss';

class ResultByProducts extends  Component {
  componentWillMount(){
    this.props.dispatch(fetchProducts('?sku='+this.props.location.query.selectedSku));
  }
  constructor(props) {
    super(props);

    const needMethodGridColumns = [
      {
        headerName: 'forecast',
        field: 'forecast',
        }, {
        headerName: 'sales',
        field: 'sales',
        }
      ];

    this.state= {
      columnDefs: needMethodGridColumns
    };
  }

  render(){
    return (
      <div className="ag-fresh">
            <div className="result-by-products-grid">
              <AgGridReact
                columnDefs={this.state.columnDefs}
                rowData={this.props.resultByProduct}
                rowSelection="multiple"
                enableSorting="true"
                enableFilter="true"
                rowHeight="25"
                headerHeight="25"
              />
            </div>
       </div>
    )
  }
}
function mapStateToProps(state) {
  return {
    resultByProduct: state.resultByProduct.resultByProducts
  };
}
export default  connect(mapStateToProps) (ResultByProducts);
