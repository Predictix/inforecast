import React from 'react';
import buildClassName from 'classnames';
import './GroupCard.scss';

class GroupCard extends React.Component {
    
    render() {
        const titleClassName = buildClassName({
            'group-card-title-BLU': this.props.groupCardTitleBLU,
            'group-card-title-GRAY': this.props.groupCardTitleGRAY
        });
        return (
 			<div className="group-card">
            	<div className={titleClassName}>
              		<div className="title-police"> {this.props.title}  </div>
            	</div>
                <div>
                	{this.props.content}
                </div>
            	<hr/>
            </div>
        );
    }
}

export default GroupCard;
