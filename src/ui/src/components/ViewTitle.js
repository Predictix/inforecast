import React from 'react';
import '../Style/ViewTitle.scss';

class ViewTitle extends React.Component {


    render() {
        return (
            <article >
                <section className="vewTitle">
                    <header>{this.props.title}</header>
                </section>
            </article>
        );
    }
}

export default ViewTitle;
