import React , { Component } from 'react';

import Logo from '../../public/inforLogo.ico';
import '../Style/Header.scss';

import { Icon,
         IconLibrary,
         Stereo,
         THEME_CONFIG
       }  from 'IR-style-guide';
       
export default class Header extends  Component {
  render(){
    return (

          <header className="header">
            <Stereo>
              <Stereo.Side alignment={Stereo.VERTICAL_ALIGNMENT.MIDDLE} orientation={Stereo.Side.ORIENTATION.LEFT}>
                <img src={Logo} alt="Inforecast" className="header_logo"/>
                <div className="header_menuicon">
                  <IconLibrary.MenuIcon size={Icon.SIZE.MEDIUM} color={THEME_CONFIG.COLOR_BY_NAME.WHITE1}/>
                </div>
              </Stereo.Side>
              <Stereo.Side alignment={Stereo.VERTICAL_ALIGNMENT.MIDDLE} orientation={Stereo.Side.ORIENTATION.RIGHT}>
              </Stereo.Side>
            </Stereo>
          </header>

    )
  }
}
