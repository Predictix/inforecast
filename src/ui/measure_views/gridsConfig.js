// jshint asi:true
/**
* Global CONFIGURATION
*/
var url = '/measure';
var factory = function(MQG){


if (typeof exports !== 'object') {
    window.serviceMeasures = measures;
}

var QueryView = MQG.QueryView;
var SpreadView = MQG.SpreadView;
var RemoveView = MQG.RemoveView;
var Measure = MQG.builder.Measure;


QueryView('resultByProduct')
  .attribute('sku', "product.sku.id")
  .attribute('store', "location.store.id")
  .attribute('week', "time.week.id")
  .measure("sales", "TOTAL")
  //.measure("forecast", "TOTAL")
  .save()


var calculate = function(d, update, ref, ratiodivide){
  var source = JSON.parse(getParam('source', d.params));
  var method = getParam('method', d.params);
  var params = {
    destination: update,
    base: ref,
    filters: d.filters
  }
  if(ratiodivide){
    params.divideMetric = ratiodivide;
  }
  var relation = method === 'percentage' && MQG.builder.Relation(source, 'FLOAT') || MQG.builder.Relation(source, MQG.metaModel.getPrimitiveType(update));
  return relation[method + 'Spread'](params);

};



MQG.addRelaxFilter('sku', 'product.sku.id')
MQG.addRelaxFilter('week', 'time.week.id')
MQG.addRelaxFilter('store', 'location.store.id')
/**
 * Response processing
 */
var formatters = {
    string: function(el) {
        if (el) {
            return el;
        } else {
            return '';
        }
    },
    number: function(el) {
        if (el) {
            return el;
        } else {
            return '0';
        }
    },
    currency: function(el) {
        if (el) {
            if (parseFloat(el) < 0) {
                return '-$' + Math.abs(parseFloat(el))
                    .toFixed(2)
                    .toString();
            } else {
                return '$' + parseFloat(el)
                    .toFixed(2)
                    .toString();
            }
        } else {
            return '$0.00';
        }

    },
    alert: function(el) {
        if (el) {
            return '!';
        }
    },
    markWithX: function(el) {
        if (el) {
            return 'x';
        }
    },
    markWithT: function(el) {
        if (el) {
            return 'T';
        }
    },
    replaceDashBySpace: function(el) {
        if (el) {
            var elwd = el.replace("_", " ");
            return elwd;
        }
    }
};

var globalPostProcessor = function(data, noAdds) {

    var format_field = function(el, formatter, fieldName, fieldName2) {
        if (fieldName in el) {
            el[fieldName] = formatter(el[fieldName], el[fieldName2]);
        }
    };
    var add_field = function(el, fieldName, fn) {
        var result = fn(el);
        if (result != null) { //jshint ignore:line
            el[fieldName] = result;
        }
    };
    var format_element = function(el, noAdds) {
        //alias
        var f = formatters;

        format_field(el, f.string, 'id');


        }

    };

};

var sort_by = function(field) {
    var key = function(x) {
        return x[field];
    };

    return function(a, b) {
        return a = key(a), b = key(b), 1 * ((a > b) - (b > a));
    };
};

var sort_by_descending = function(field) {
    var key = function(x) {
        return x[field];
    };

    return function(b, a) {
        return a = key(a), b = key(b), 1 * ((a > b) - (b > a));
    };
};

var sort_by_multiple = function(fields) {
    return function(a, b) {
        var i = 0, l = fields.length, result = 0;

        while (!result && i < l) {
          var field = fields[i],
            f1 = a[field], f2 = b[field];

          result = 1 * ((f1 > f2) - (f2 > f1));
          i++;
        }

        return result;
    }
}

var processAndSortWithNoAdds = function(field, data) {
    data = globalPostProcessor(data, true);
    return data.sort(sort_by(field));
};

var processAndSort = function(field, data) {
    var sortFunc = field instanceof Array ? sort_by_multiple : sort_by;
    data = globalPostProcessor(data);
    return data.sort(sortFunc(field));
};

// export as a module
if (typeof exports === 'object') {
    module.exports = factory;
}
else {
    window.measureServiceUrl = url;
    window.gridsConfig = factory;
    window.measurePostProcess = {
      processAndSort: processAndSort
    };
}
