This repository contains the configurable forecast manager , Inforecast.

# Prerequisites

## Hologram me
To make sure that your aws creds are all setup, you need to run the following command:
```
hologram me
```

## Install Nix
To build the application locally, your machine has to be equiped with Nix package manager.
To install Nix, run the following command:

```
bash <(curl https://nixos.org/nix/install)
```

It is best to be using the same version of the Nix Packages collection. Currently, we are using version -16.09-.

To check the version that you're using, run the following:

```
nix-instantiate --eval '<nixpkgs>' -A lib.nixpkgsVersion
```

If you are not using the version 16.09, you can update that by running the following commands:

```
nix-channel --remove `nix-channel --list | cut -d ' ' -f1`
nix-channel --add https://nixos.org/channels/nixos-16.09 nixpkgs
nix-channel --update
```
For further details on how to build using nix you can take a look to this documentation

```
https://logicblox.atlassian.net/wiki/pages/viewpage.action?spaceKey=WBA&title=Building+walgreens_backend+using+Nix
```
## Download builder config

Clone the repository https://bitbucket.org/logicblox/builder-config in your machine. Then extend the environment variable NIX_PATH with the path of the repository as mentioned below. Please -change- the path to builder-config accordingly:

```
export NIX_PATH="config=/yourdirectory/builder-config":$NIX_PATH
```

You can add those lines to the ``~/.bashrc`` file (if you are using a Linux distribution) so that you don't have to re-run that command with every terminal session.


## Download lb devops

Clone the repository https://bitbucket.org/logicblox/lbdevops in your machine. Then extend the environment variable NIX_PATH with the path of the repository as mentioned below. Please -change- the path to lbdevops accordingly:

```
export NIX_PATH="lbdevops=/yourdirectory/lbdevops":$NIX_PATH
```

You can add those lines to the ``~/.bashrc`` file (if you are using a Linux distribution) so that you don't have to re-run that command with every terminal session.

## Download lamias-servicetester

Clone the repository bitbucket.org/Predictix/lamias-servicetester in your machine under the same location as the inforecast repository.

## Download measure-query-gen

Clone the repository bitbucket.org/logicblox/measure-query-gen in your machine under the same location as the inforecast repository.


# Project structure
```
|-- conf/
|-- config
|-- nix
|-- scripts
|-- src/
|   |--common/
|   |--config/
|   |--fe/
|   |--frontend/
|   |--logiql/
|   |--modeler/
|   |--rules/
|   |--staging/
|   |--ui/
|   |--workflow/
|
|--tests/
|
```
* conf 

contains nginx config files.

* config 

contains lb-web-server config files and netmap files.

* nix

This folder contains  nix files needed to deploy our project on remote enviroments.

* scripts
Contains scripts needed to build the modeler workspace and some tools

* src 
This is where the meat of inforecast will live. We have ten subfolders in here

- logicql: contains business logic related to the forecast manager.
- fe : will contain the forecast engine code base.
- modeler: configuration for the modeler workspace.
- config: contains config files to generate the model, sheets and navigations for the modeler app
- common: contains shared blocs between the different workspaces of our app.
- ui: contains the user interface of the forecast manager.
- fronend: contains the  user interface of the modeler app.




# Manual Build

You can access the build environment by running:

```
nix-shell -A build
```

to exist the build environment, simply run

```
exit
```

In the build environment, the build process is performed manually. These are the steps:

## Start LogicBlox services

The next step requires LB services such as connectblox and the page manager to
be running. If you have not previously started these services, e.g., by launching
them from within another terminal window, you will need to do so now. Do so with
the following command:

```
lb services start
```

## Build the workspace

You must first instantiate the build configuration. This step generates
a Makefile from which to issue the actual build commands. Do the following:

```
lb config
```

At this point, you can use 'make' in the normal way to iterate during development.
To build the application, run: 
```
make
make install
./install.sh --local
```

To build a clean version of the application, you can run

```
make clean
make
make install
./install.sh --local
make deploy
```

## Build the UI

To build the UI with Infor styleguide library, you'll need to have access to Artifactory. 
Contact joseph.anderson@infor.com to setup an account. Once you have an artifactory account, go to https://artifactory.clr.awsdev.infor.com/artifactory/webapp/#/profile , grab the encrypted password as shown in the picture below, you'll need when running the artifactory setup script.

![Scheme](doc/img/artifactory.png)

```
cd src/ui/src
npm install
npm start # Will start the development server
```

## build the modeler UI

```
make IP-workbook
make start-nginx
```

## run tests

```
make check
```

## update tests

```
make update-tests
```

