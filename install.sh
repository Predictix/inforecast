#! /usr/bin/env bash  

set -e
set -x

set_up_workflows()
{
     
    #------------------------------
    # Create workflow workspaces
    #------------------------------

    echo "------------ START: Setting up /workflow ------------"

    echo "Creating workflow workspace ..."
    lb workflow create --overwrite --workspace /workflow
    lb addblock /workflow -f ${BASEDIR}/workflow/variables.logic

    lb workflow install --file ${BASEDIR}/workflow/main/bootstrap_network_manager.wf --name bootstrap_nm --extension ${BASEDIR}/network-manager
    lb workflow install --file ${BASEDIR}/workflow/dev_deploy.wf --name dev_deploy  --extension ${BASEDIR}/network-manager
       
    
    lb exec /workflow -f $TMP

}

BASEDIR=$(dirname $(readlink -f $0))
TMP=`mktemp -t logiQl.$$.XXXXXXXXXX`
echo " ^target_dir[]=\"$PWD/out\"." > $TMP
echo " ^network_path[]=\"$PWD/out/network-manager\"." >> $TMP
echo " ^workspaces[]=\"$PWD/out/workspaces\"." >> $TMP
echo " ^config_dir[]=\"$PWD/out/config\"." >> $TMP
echo " ^extra_config_dir[]=\"$PWD/out/config\"." >> $TMP
echo " ^stagenode[]=\"localhost\"." >> $TMP
echo " ^gen_zero[]=\"20160803\"." >> $TMP
echo " ^data_dir[]=\"$PWD/data\"." >> $TMP    
echo " ^platform[]=\"4.4.2\"." >> $TMP

OPTION=$1
case $OPTION in
  --frontend) echo "Remote machine deployment"
    mkdir -p /data/lb_deployment/uncompressed-workspaces
    cp -r $(dirname $(readlink -f $0))/workspaces/* /data/lb_deployment/uncompressed-workspaces
    TMP=`mktemp -t logiQl.$$.XXXXXXXXXX`
    echo " ^target_dir[]=\"/data/lb_deployment/installed-app\"." > $TMP
    echo " ^network_path[]=\"/data/lb_deployment/installed-app/network-manager\"." >> $TMP
    echo " ^workspaces[]=\"/data/lb_deployment/uncompressed-workspaces\"." >> $TMP
    echo " ^config_dir[]=\"/data/lb_deployment/installed-app/config\"." >> $TMP
    echo " ^extra_config_dir[]=\"/data/lb_deployment/installed-app/config\"." >> $TMP
    echo " ^stagenode[]=\"localhost\"." >> $TMP
    echo " ^platform[]=\"4.4.2\"." >> $TMP
    ;;
  --local)
    BASEDIR="$PWD/out"
    ;;
  *) echo "Local machine deployment"
esac

set_up_workflows