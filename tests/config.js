process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
lamias.config({
    'Relax.home': process.env.MEASURE_QUERY_GEN_HOME,
    'Relax.views': process.env.MEASURE_QUERY_GEN_VIEWS,
    'Relax.URL': 'http://localhost:8080/measure',
    'login.URL': 'http://localhost:8080/login'
});