require('./config');
var filters = require('./helpers');
lamias
.add(Suite('result_by_product')
        .add(Relax('filters')
            .url('/query/resultByProduct')
            .level('product','sku')
            .level('location','store')
            .level('time','week')
            )
        )
