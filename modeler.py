from lbconfig.api import *

'''
lb-config support for building modeler projects

The idea is that projects can configure modeler builds simply by
calling `lbconfig.configure(app-prefix)`.

The resulting make file with then contain targets that can be used to build
up modeler applications. Some targets:

    - view-warmup: generates view warmup queries
    - rule-warmup: generates rule warmup queries
    - warmup-rules: submits rule warmup queries
    - warmup-views: submits view warmup queries

These targets all have a dependency to the modeler sources, so they will
automatically copy over any necessary library or script from the $LB_MODELER_HOME.
'''

# directories where we can find modeler assets and scripts
_default_js_scripts_src_dir = '$(modelerjs)/lib/js'
_default_assets_src_dir = '$(modelerjs)/assets'
_default_js_lib_src_dir = os.path.join(_default_assets_src_dir, 'js')
_default_python_dir = '$(modelerjs)/python'

# directories to where we will copy javascript assets and scripts
_default_js_scripts_dest_dir = 'scripts/js'
_default_js_lib_dest_dir = 'node_modules'

modeler_projects={}

rule_warmup_query_dir = '$(build)/srcgen/rule-warmup'
view_warmup_query_dir = '$(build)/srcgen/view-warmup'

variable('view_query_dir', view_warmup_query_dir)

class ModelerProject(object):
    def __init__(self, name, config_dir, composite_aggs = None, composite_spreads = None, dimensions = None, hierarchies = None, intersections = None, levels = None, measures = None, libraries = []):
        self.csv_copy_dir='$(build)/cleancsvs/%s'%name
        self.srcgen_dir='$(build)/srcgen/logiql/'
        self.pp_rules_dir='$(build)/srcgen/rules/%s'%name
        self.warmup_rules_dir='$(build)/srcgen/rule-warmup/%s'%name
        self.config_dir=config_dir
        self.name=name
        self.libraries=libraries
        
        self.generated_files = ['$(build)/srcgen/logiql/%s.project'%name,
                                '$(build)/srcgen/logiql/%s/levelmaps.logic'%name,
                                '$(build)/srcgen/logiql/%s/levels.logic'%name,
                                '$(build)/srcgen/logiql/%s/measures.logic'%name]
        
        self.csv_files = ['%s/CompositeAggs.csv'%(composite_aggs if composite_aggs!=None else config_dir),
                          '%s/CompositeSpreads.csv'%(composite_spreads if composite_spreads!=None else config_dir),
                          '%s/Dimensions.csv'%(dimensions if dimensions!=None else config_dir),
                          '%s/Hierarchies.csv'%(hierarchies if hierarchies!=None else config_dir),
                          '%s/Intersections.csv'%(intersections if intersections!=None else config_dir),
                          '%s/Levels.csv'%(levels if levels!=None else config_dir),
                          '%s/Measures.csv'%(measures if measures!=None else config_dir)]
        
        self.command='python scripts/modeler/generate_schema_from_config.py %s build/srcgen build/cleancsvs/%s --module %s ' %(config_dir, name, name)
        for argument in ['composite_aggs', 'composite_spreads', 'dimensions', 'hierarchies', 'intersections', 'levels', 'measures']:
            if eval(argument)!=None:
                self.command+='--%s %s '%(argument, eval(argument))
        if self.libraries!=[]:
            self.command+='--libraries %s'%(' '.join(self.libraries))

    def generate(self):
        for generated_file in self.generated_files:
          rule(output=generated_file,
               input=self.csv_files,
               commands=[self.command])


def modeler_library(name, config_dir, composite_aggs = None, composite_spreads = None, dimensions = None, hierarchies = None, intersections = None, levels = None, measures = None, libraries = []):

    project=ModelerProject(name, config_dir, composite_aggs, composite_spreads, dimensions, hierarchies, intersections, levels, measures, libraries)
    project.generate()
    deps={'lb_web': '$(lb_web)',
          'lb_measure_service': '$(lb_measure_service)',
          'modeler_config': '$(modelerjs)',
          'pivot': '$(modelerjs)'}
    for lib in libraries:
        deps[lib]=modeler_projects[lib].srcgen_dir

    lb_library(name=name,
               srcdir='$(build)/srcgen/logiql',
               generated=True,
               srcgen=['%s.project'%name,
                       '%s/levelmaps.logic'%name,
                       '%s/levels.logic'%name,
                       '%s/measures.logic'%name],
               deps=deps)

    modeler_projects[name]=project

def configure(app_prefix, app_base_dir='.',
              js_lib_src_dir=_default_js_lib_src_dir,
              js_lib_dest_dir=_default_js_lib_dest_dir,
              assets_dir=_default_assets_src_dir,
              js_scripts_dest_dir=_default_js_scripts_dest_dir,
              js_scripts_src_dir=_default_js_scripts_src_dir,
              python_dir=_default_python_dir,
              libraries=[]):
    '''This is the main configuration target that will configure modeler
    for applications.

    For applications using the platform build, it is not necessary to provide
    paramter values different from the default values.

    @param app_prefix
        The application prefix, e.g., 'your-app'
    @param js_lib_dir
        The directory containing modeler javascript libraries
    @param js_scripts_dir
        The directory containing modeler javascript scripts
    @param python_dir
        The directory containing modeler python scripts
    '''

    assets(assets_dir=assets_dir)
    js_scripts(js_scripts_src_dir=js_scripts_src_dir, js_scripts_dest_dir=js_scripts_dest_dir)
    js_libraries(js_lib_src_dir=js_lib_src_dir, js_lib_dest_dir=js_lib_dest_dir)
    if libraries != []:
        rule_warmup_queries_by_lib(app_base_dir, libraries, python_dir)
    else:
        rule_warmup_queries(app_base_dir=app_base_dir, python_dir=python_dir, config_dir='src/config')
    view_warmup_queries(app_prefix, app_base_dir,
                        js_lib_dest_dir=js_lib_dest_dir, js_scripts_dest_dir=js_scripts_dest_dir)
    rule_warmup(app_prefix,
                js_lib_dest_dir=js_lib_dest_dir, js_scripts_dest_dir=js_scripts_dest_dir)
    view_warmup(app_prefix,
                js_lib_dest_dir=js_lib_dest_dir, js_scripts_dest_dir=js_scripts_dest_dir)


def js_libraries(js_lib_src_dir=_default_js_lib_src_dir, js_lib_dest_dir=_default_js_lib_dest_dir):
    '''Copies modeler js libraries to the project dir

    @param js_lib_dir
        The directory from which we will copy the modeler javascript libraries,
        e.g. logicblox.js, logicbloxtestutils.js, from.
    '''

    files = ['logicbloxtestutils.js']
    for f in files:
        _copy_file(f, js_lib_src_dir, js_lib_dest_dir)


def js_scripts(js_scripts_src_dir=_default_js_scripts_src_dir, js_scripts_dest_dir=_default_js_scripts_dest_dir):
    '''Copies modeler js scripts to the project dir

    @param js_scripts_dir
        The directory from which we will copy the modeler javascript scripts,
        e.g. submitqueries.js, generateviewwarmupqueries.js, from.
    '''

    files = ['submitqueries.js',
             'generateviewwarmupqueries.js']

    for f in files:
        _copy_file(f, js_scripts_src_dir, js_scripts_dest_dir)


def assets(assets_dir=_default_assets_src_dir):
    '''Copies modeler html assets to the project dir

    @param assets_dir
        The directory from which we will copy the modeler assets from.
    '''
    rule(output=['frontend/js/logicblox.js'],
         input=[os.path.join(assets_dir, 'js', 'logicblox.js')],
         commands=['mkdir -p frontend',
                   'cp -R %s/* frontend' % assets_dir,
                   'chmod -R a+w frontend'],
         # we needs this to by phony because we can't rely on the date
         # of the assets being newer than in the project's source files
         phony=True)

    rule(
        output='all',
        input=['frontend/js/logicblox.js'])

    install_dir('frontend', 'static')

    rule(
        output='frontend',
        input=['frontend/js/logicblox.js'])


def rule_warmup_queries(app_base_dir, python_dir=_default_python_dir, config_dir='src/config'):
    '''Creates 'rule-queries' target that will create a query to
    warm up rules in $(build)/srcgen/rule-warmup/measure_rules.json

    @param python_dir
        The directory containing the modeler python scripts'''

    rule(
        output=[os.path.join(rule_warmup_query_dir, 'measure_rules.json'), 'rule-queries'],
        input='',
        commands=[
            'mkdir -p %s' % rule_warmup_query_dir,
            'python %s/generate_warmup_from_config.py %s/%s $(build)/srcgen/rule-warmup' % (python_dir, app_base_dir, config_dir)])

    install_dir(rule_warmup_query_dir, 'build/srcgen/rule-warmup')


def rule_warmup_queries_by_lib(app_base_dir, libraries, python_dir=_default_python_dir):
    '''Creates 'rule-queries' target that will create a query to
    warm up rules in $(build)/srcgen/rule-warmup/measure_rules.json

    @param python_dir
        The directory containing the modeler python scripts'''
    for lib in libraries:
        rule(
            output=os.path.join(modeler_projects[lib].warmup_rules_dir, 'measure_rules.json'),
            input='',
            commands=[
                'mkdir -p %s' % rule_warmup_query_dir,
                'python %s/generate_warmup_from_config.py %s %s' % (python_dir, modeler_projects[lib].csv_copy_dir, modeler_projects[lib].warmup_rules_dir)])

    install_dir(rule_warmup_query_dir, 'build/srcgen/rule-warmup')

    target('rule-queries',
           [os.path.join(modeler_projects[lib].warmup_rules_dir, 'measure_rules.json') for lib in libraries])


def view_warmup_queries(app_prefix, app_base_dir,
                        js_scripts_dest_dir=_default_js_scripts_dest_dir,
                        js_lib_dest_dir=_default_js_lib_dest_dir):
    '''Creates a 'view-queries' target that will create query files
    to warmup up views. Files will be added to $(build)/srcgen/view-warmup

    @param app_prefix
        The application prefix, e.g., 'your-app'
    '''

    variable('canvas_dir', '%s/src/config/canvas' % app_base_dir)

    generate_script = os.path.join(js_scripts_dest_dir, 'generateviewwarmupqueries.js')
    rule(
        output='view-queries',
        input=[
            generate_script,
            os.path.join(js_lib_dest_dir, 'logicbloxtestutils.js')],
        commands=[
            'mkdir -p $(view_query_dir)',
            'node %s --app-prefix %s --config-dir $(canvas_dir) $(view_query_dir)' % (generate_script, app_prefix)])

    install_dir(view_warmup_query_dir, 'build/srcgen/view-warmup')


def view_warmup(app_prefix,
                js_scripts_dest_dir=_default_js_scripts_dest_dir,
                js_lib_dest_dir=_default_js_lib_dest_dir):
    '''Creates a 'warmup-views' target that will submit view queries to
    warmup the workspace

    @param app_prefix
        The application prefix, e.g., 'your-app'
    '''

    viewwarmup_script = os.path.join(js_scripts_dest_dir, 'submitqueries.js')
    rule(
        output='warmup-views',
        input=[
            os.path.join(js_lib_dest_dir, 'logicbloxtestutils.js'),
            viewwarmup_script],
        commands=['node %s --fail-on-error --app-prefix %s $(view_query_dir)' % (
            viewwarmup_script, app_prefix)],
        phony=True)

    
def rule_warmup(app_prefix,
                js_scripts_dest_dir=_default_js_scripts_dest_dir,
                js_lib_dest_dir=_default_js_lib_dest_dir):
    '''Creates a 'warmup-rules' target that will submit queries to
    warmup the rules in a workspace

    @param app_prefix
        The application prefix, e.g., 'your-app'
    '''

    viewwarmup_script = os.path.join(js_scripts_dest_dir, 'submitqueries.js')
    rule(
        output='warmup-rules',
        input=[
            os.path.join(js_lib_dest_dir, 'logicbloxtestutils.js'),
            viewwarmup_script],
        commands=['node %s --fail-on-error --app-prefix %s $(build)/srcgen/rule-warmup' % (
            viewwarmup_script, app_prefix)],
        phony=True)


def _copy_file(filename, src_dir, dest_dir):
    src_file = os.path.join(src_dir, filename)
    dest_file = os.path.join(dest_dir, filename)
    # we need to use our own copy target because
    # we need to create the directory too
    rule(output=dest_file,
         input=[src_file],
         commands=[
             'mkdir -p %s' % dest_dir,
             'cp %s %s' % (src_file, dest_dir)])
    rule(output='all', input=[dest_file])
    emit_clean_file(dest_file)


def _csv_config_files(dir):
    return [
        '%s/CompositeAggs.csv' %dir,
        '%s/CompositeSpreads.csv' %dir,
        '%s/Dimensions.csv' %dir,
        '%s/Hierarchies.csv' %dir,
        '%s/Intersections.csv' %dir,
        '%s/Levels.csv' %dir,
        '%s/Measures.csv' %dir
    ]

def _generated_files(dir):
    return ['%s/generated_schema/levelmaps.logic' %dir,
            '%s/generated_schema/levels.logic' %dir,
            '%s/generated_schema/measures.logic' %dir,
            '%s/generated_schema.project' %dir]


def generate_schema_from_config(csv_dir,srcgen_dir,csv_copy_dir='',input=[]):
    '''Creates targets to generate logiql files from CSV spec by running the 
    script generate_schema_from_config.py.

    @param csv_dir
        Path containing the CSV files to generate logic from.
    @param srcgen_dir
        Output path for the generated logic.
    @param csv_copy_dir
        Path to contain a copy of the processed CSVs from csv_dir.
    @param input
        Targets these rules depend on.
    '''
    for generated_file in _generated_files('%s/logiql' %srcgen_dir)+_csv_config_files(csv_copy_dir):
      rule(output=generated_file,
           input=_csv_config_files(csv_dir) + input,
           commands=['python $(modelerjs)/python/generate_schema_from_config.py %s %s %s' %(csv_dir, srcgen_dir, csv_copy_dir)]
          )

def create_workbook(name,template,commands,extra_inputs=[]):
    '''Creates create-workbooks-<name> targets to create workbooks by runnings 
    'commands'. The rule will have an input dependency on the workbook's 
    template, which needs to have been created using create_template function.

    @param name
        Workbook name.
    @param template
        Workbook template.
    @param commands
        Commands to be run to create the workbook, e.g., running a workflow.
    @param extra_inputs
        Targets this rule depends on.
    '''
    rule(output='$(build)/create_workbooks_%s.success'%name,
        input=['$(build)/create_template_%s.success'%template],
        commands=commands+['touch $(build)/create_workbooks_%s.success'%name])
    
    rule('create-workbooks-%s'%name, ['$(build)/create_workbooks_%s.success'%name], phony=True)


def create_template(json_spec, workspace, extra_inputs=[]):
    '''Creates create-template-<template_name> targets to install templates in 
    a workspace or a branch. If the template will be installed in a branch, 
    then a dependency for that branch to have been created will be added to the
    rule's input,  the branch in question should be created using 
    branch_workspace function.

    @param json_spec
        Path containing the template's definition JSON file, the name of the 
        file will be used as the template's name.
    @param workspace
        Workspace name.
    @param extra_inputs
        Targets this rule depends on.
    '''
    template_name=json_spec.split('/')[-1].split('.')[0]
    if '@' in workspace:
        workspace_only=workspace.split('@')[0]
        branch=workspace.split('@')[1]
        extra_inputs+=['$(build)/branch_%s_from_%s.success'%(branch, workspace_only)]

    rule(output='$(build)/create_template_%s.success'%template_name,
         input=extra_inputs,
         commands=['$(lb) workbook create-template -w %s -j %s'%(workspace, json_spec),
                   'touch $(build)/create_template_%s.success'%template_name])

    rule('create-template-%s'%template_name, ['$(build)/create_template_%s.success'%template_name], phony=True)

def branch_workspace(workspace, branch, extra_inputs=[]):
    '''Creates branch-<branch_name> targets to create a branch from a workspace. 

    @param workspace
        Workspace to branch from.
    @param branch
        branch name.
    @param extra_inputs
        Targets this rule depends on.
    '''
    rule(output='$(build)/branch_%s.success'%branch,
         input=extra_inputs,
         commands=['$(lb) branch %s %s'%(workspace, branch),
                   'touch $(build)/branch_%s.success'%branch])

    rule('branch-%s'%branch,['$(build)/branch_%s.success'%branch],phony=True)
